<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'HomeController@index');
$router->get('/preloader', 'ShareController@preloader');
$router->post('/share-fb', 'ShareController@fb');
$router->post('/get-data', 'DataController@index');
$router->post('/set-download', 'ShareController@setDownload');

$router->get('/statistics', 'StatisticsController@index');
$router->get('/statistics-login', 'StatisticsController@getLogin');
$router->post('/statistics-login', 'StatisticsController@login');

$router->get('/clean-share-data', function(){
    \Illuminate\Support\Facades\Artisan::call('share:clean');
});

