<?php

namespace App\Http\Controllers;

use App\Classes\Statistics;
use App\Models\Share;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    /**
     * @var Statistics
     */
    public $statistics;

    public function __construct(Statistics $statistics)
    {
        $this->statistics = $statistics;
    }

    public function fb(Request $request)
    {
        if ($request->method('ajax') && $request->hasFile('file')) {
            $file = $request->file('file');

            $tmpDir = uniqid(time());

            mkdir('/tmp/' . $tmpDir,0775);

            $file->move('tmp/' . $tmpDir, $file->getClientOriginalName());

            $url = url('/') . '?hash=' . $tmpDir;

            $share         = new Share();
            $share->hash   = $tmpDir;
            $share->ip     = $request->ip();
            $share->orient = $request->get('orient');
            $share->save();

            $redirect = 'https://www.facebook.com/dialog/share?app_id=119877023392301&display=popup&href=' . $url;

            $this->statistics->setShare($request->ip(),$tmpDir);

            return [
                'status'   => true,
                'redirect' => $redirect,
                'hash'     => $tmpDir
            ];
        }
    }

    public function preloader()
    {
        return view('share.preloader', []);
    }

    public function setDownload(Request $request)
    {
        if ($request->method('ajax')) {
            $this->statistics->setDownload($request->ip());
        }
    }
}
