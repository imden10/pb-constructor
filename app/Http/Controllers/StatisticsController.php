<?php

namespace App\Http\Controllers;

use App\Classes\Statistics;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    /**
     * @var Statistics
     */
    public $statistics;

    public function __construct(Statistics $statistics)
    {
        $this->statistics = $statistics;
        session_start();
    }

    public function getLogin()
    {
        return view('statistics.login');
    }

    public function login(Request $request)
    {
        $password = $request->get('password_field');

        if ($password && $password === env('STATISTICS_PASSWORDS')) {
            $_SESSION['password_entered'] = true;
            return redirect('/statistics');
        }

        return redirect('/statistics-login');
    }

    public function index(Request $request)
    {
        if (!isset($_SESSION['password_entered'])) {
            return redirect('/statistics-login');
        }

        // пользователей на сайте сейчас
        $online = $this->statistics->getOnline();

        // Общее количество посищений
        $visitAll = $this->statistics->getVisits();

        // Общее количество уникльных посищений
        $visitAllUnique = $this->statistics->getVisitsUnique();

        // Количество посищений за сегодня
        $visitToDay = $this->statistics->getVisitsToDay();

        // Количество уникальных посищений за сегодня
        $visitUniqueToDay = $this->statistics->getVisitsUniqueToDay();

        // Количество кликов на репост
        $share = $this->statistics->getShare();

        // Количество кликов на скачать
        $download = $this->statistics->getDownload();

        return view('statistics.index', [
            'online'           => $online,
            'visitAll'         => $visitAll,
            'visitAllUnique'   => $visitAllUnique,
            'visitToDay'       => $visitToDay,
            'visitUniqueToDay' => $visitUniqueToDay,
            'share'            => $share,
            'download'         => $download
        ]);
    }

}
