<?php

namespace App\Http\Controllers;

use App\Models\Colors;
use App\Models\Stickers;
use App\Models\Templates;
use App\Models\Texts;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function index(Request $request)
    {
        $templates = Templates::query()->with('props')
            ->orderBy('sort','asc')
            ->get();

        $stickers  = Stickers::query()->get();
        $colors    = Colors::query()->get();
        $texts     = Texts::query()->get();

        $path = '/img/';

        $tabsContent = [];

        $resTemplates = [];
        $resStickers  = [];
        $resColors    = [];
        $resTexts     = [];

        foreach ($templates as $key => $template) {
            $resTemplates[$key] = [
                'type'   => 'template',
                'name'   => $template->name,
                'orient' => $template->orient,
                'group'  => $template->group,
                'bg'     => $path . $template->bg,
                'src'    => $path . $template->src
            ];

            if (count($template->props)) {
                foreach ($template->props as $prop) {
                    $resTemplates[$key]['props']['textItems'][] = [
                        'name'  => $prop->name,
                        'width' => (int)$prop->width,
                        'angle' => (int)$prop->angle,
                        'pos'   => [
                            'x' => (int)$prop->pos_x,
                            'y' => (int)$prop->pos_y,
                        ]
                    ];
                }
            }
        }

        foreach ($stickers as $sticker) {
            $resStickers[] = [
                'type' => 'sticker',
                'name' => $sticker->name,
                'src'  => $path . $sticker->src,
            ];
        }

        foreach ($colors as $color) {
            $resColors[] = [
                'type'  => 'color',
                'name'  => $color->name,
                'color' => $color->color,
            ];
        }

        foreach ($texts as $text) {
            $resTexts[] = [
                'type' => 'text',
                'name' => $text->name,
                'src'  => $path . $text->src,
            ];
        }

        $tabsContent[] = $resTemplates;
        $tabsContent[] = $resStickers;
        $tabsContent[] = $resColors;
        $tabsContent[] = $resTexts;

        return $tabsContent;
    }
}
