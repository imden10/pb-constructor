<?php

namespace App\Http\Controllers;

use App\Classes\Statistics;
use App\Models\Share;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var Statistics
     */
    public $statistics;

    public function __construct(Statistics $statistics)
    {
        $this->statistics = $statistics;
    }

    public function index(Request $request)
    {
        $this->statistics->setOnline($request->ip());
        $this->statistics->setVisit($request->ip());

        $hash = $request->get('hash') ?? null;
        $share = null;

        if($hash){
            $share = Share::query()->where('hash', $hash)->first();
        }

        return view('home.index',[
            'share' => $share
        ]);
    }
}
