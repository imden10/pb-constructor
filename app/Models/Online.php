<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Online extends Model
{
    protected $table = 'online';

    protected $guarded = [];

    public $timestamps = false;
}
