<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stickers extends Model
{
    protected $table = 'stickers';

    protected $guarded = [];
}
