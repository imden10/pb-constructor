<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Texts extends Model
{
    protected $table = 'texts';

    protected $guarded = [];
}
