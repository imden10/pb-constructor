<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WillShare extends Model
{
    protected $table = 'will_share';

    protected $guarded = [];
}
