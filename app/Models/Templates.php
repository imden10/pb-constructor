<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $table = 'templates';

    protected $guarded = [];

    public function props()
    {
        return $this->belongsToMany(Props::class);
    }
}
