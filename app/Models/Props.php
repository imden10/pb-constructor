<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Props extends Model
{
    protected $table = 'props';

    protected $guarded = [];
}
