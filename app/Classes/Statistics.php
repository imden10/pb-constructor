<?php


namespace App\Classes;

use App\Models\Online;
use App\Models\Visits;
use App\Models\WillShare;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Statistics
{
    /**
     * @param $ip
     */
    public function setOnline($ip): void
    {
        $valid = preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $ip);

        if (!$valid)
            return;

        // точность он-лайн (секунды); время, в течении которого
        // пользователя, зашедшего на страничку, мы считаем находящимся
        // на сайте
        $wine = 180;

        // удаляем всех, кто уже пробыл $wine секунд или у кого ИП текущий
        Online::query()
            ->where('unix', '<', time() - $wine)
            ->orWhere('ip', $ip)
            ->delete();

        // вставляем свою запись
        $online       = new Online();
        $online->ip   = $ip;
        $online->unix = time();
        $online->save();
    }

    /**
     * @return int
     */
    public function getOnline(): int
    {
        return Online::query()->count();
    }

    public function setVisit($ip): void
    {
        $date = Carbon::today()->format('Y-m-d');

        Visits::updateOrCreate(
            [
                'ip'   => $ip,
                'date' => $date
            ],
            [
                'ip'    => $ip,
                'count' => DB::raw('count+1'),
                'date'  => $date
            ]
        );
    }

    /**
     * Общее количество посищений
     *
     * @return int
     */
    public function getVisits(): int
    {
        $count = 0;

        $visits = Visits::query()->get();

        foreach ($visits as $visit) {
            $count += $visit->count;
        }

        return $count;
    }

    /**
     * Общее количество уникльных посищений
     *
     * @return int
     */
    public function getVisitsUnique(): int
    {
        return Visits::query()
            ->get()
            ->groupBy('ip')
            ->count();
    }

    /**
     * Количество посищений за сегодня
     *
     * @return int
     */
    public function getVisitsToDay(): int
    {
        $count = 0;

        $visits = Visits::query()
            ->where('date', Carbon::today()->format('Y-m-d'))
            ->get();

        foreach ($visits as $visit) {
            $count += $visit->count;
        }

        return $count;
    }

    /**
     * Количество уникальных посищений за сегодня
     *
     * @return int
     */
    public function getVisitsUniqueToDay(): int
    {
        return Visits::query()
            ->where('date', Carbon::today()->format('Y-m-d'))
            ->get()
            ->groupBy('ip')
            ->count();
    }

    /**
     * Увеличить количество кликов на репост
     *
     * @param $ip
     * @param $hash
     */
    public function setShare($ip, $hash): void
    {
        WillShare::Create([
                'ip'    => $ip,
                'share' => 1,
                'hash'  => $hash
            ]);
    }

    /**
     * Количество кликов на репост
     *
     * @return int
     */
    public function getShare(): int
    {
        return WillShare::query()
            ->where('share',1)
            ->count();
    }

    /**
     * Увеличить количество скачиваний
     *
     * @param $ip
     */
    public function setDownload($ip): void
    {
        WillShare::Create([
                'ip'       => $ip,
                'download' => 1,
            ]);
    }

    /**
     * Количество кликов на скачать
     *
     * @return int
     */
    public function getDownload(): int
    {
        return WillShare::query()
            ->where('download',1)
            ->count();
    }
}
