<?php

namespace App\Console\Commands;

use App\Models\Share;
use Illuminate\Console\Command;

class CleanShare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'share:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean unused files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Clean unused files start<br>";
        $days = 2;

        $share = Share::query()
            ->whereDate('created_at', '<=', date('Y-m-d H:i:s', time() - $days * 60 * 60 * 24))
            ->get();

        if(count($share)){
            foreach ($share as $item){
                if(file_exists(base_path() . '/public/tmp/' . $item->hash . '/temp.png')){
                    unlink(base_path() . '/public/tmp/' . $item->hash . '/temp.png');
                    rmdir (base_path() . '/public/tmp/' . $item->hash . '/');
                }
                echo $item->hash . ' - deleted<br>';
                $item->delete();
            }
        }
        else {
            echo "There is nothing" . PHP_EOL;
        }
    }
}
