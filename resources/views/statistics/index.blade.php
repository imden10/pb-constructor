<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="/favicon.ico">
    <title>#ОбираєшТи - Статистика</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body {
            font-size: 18px;
            font-family: 'Roboto', sans-serif;
        }

        strong {
            font-size: 18px;
            font-family: 'Roboto', sans-serif;
            font-weight: 600;
        }
    </style>
</head>
<body>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/statistics">#ОбираєшТи - Статистика</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Сколько людей на сайте сейчас</td>
                    <td><strong>{{$online}}</strong></td>
                </tr>
                <tr>
                    <td>Общее количество посещений</td>
                    <td><strong>{{$visitAll}}</strong></td>
                </tr>
                <tr>
                    <td>Общее количество уникальных посещений</td>
                    <td><strong>{{$visitAllUnique}}</strong></td>
                </tr>
                <tr>
                    <td>Количество посещений за сегодня</td>
                    <td><strong>{{$visitToDay}}</strong></td>
                </tr>
                <tr>
                    <td>Количество уникальных посещений за сегодня</td>
                    <td><strong>{{$visitUniqueToDay}}</strong></td>
                </tr>
                <tr>
                    <td>Количество репостов</td>
                    <td><strong>{{$share}}</strong></td>
                </tr>
                <tr>
                    <td>Количество скачиваний</td>
                    <td><strong>{{$download}}</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
