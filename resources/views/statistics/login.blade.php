<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="/favicon.ico">
    <title>#ОбираєшТи - Статистика</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;600&display=swap" rel="stylesheet">
    <style>
        body {
            font-size: 18px;
            font-family: 'Roboto', sans-serif;
        }

        .center-container {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 50vh;
        }
    </style>
</head>
<body>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/statistics">#ОбираєшТи - Статистика</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="center-container">
                <form method="POST" action="/statistics-login" class="form-inline">
                    <div class="form-group">
                        <input type="password" class="form-control" id="fieldPassword" name="password_field" placeholder="Пароль" required>
                    </div>
                    <input type="submit" class="btn btn-success" value="Вход">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
