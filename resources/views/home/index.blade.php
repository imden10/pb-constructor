<!DOCTYPE html>
<html lang="">
<head>
    @if($share)
    <meta property="fb:app_id" content="119877023392301"/>
    <meta property="og:url" content="{{app('url')->full()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="#ОбираєшТи"/>
    <meta property="og:description" content="Відмічай трьох друзів, кому даруєш унікальну листівку"/>
    <meta property="og:image" content="{{url('/tmp') . '/' . $share->hash . '/temp.png'}}"/>
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="{{$share->orient === 'h' ? '600' : '315'}}" />
    <meta property="og:image:height" content="{{$share->orient === 'h' ? '315' : '600'}}" />
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="/favicon.ico">
    <title>#ОбираєшТи</title>

        <link href="/css/app.394c65fc.css" rel="preload" as="style">
        <link href="/js/app.09e56caf.js" rel="preload" as="script">
        <link href="/js/chunk-vendors.7ec8c6c4.js" rel="preload" as="script">
        <link href="/css/app.394c65fc.css" rel="stylesheet">

</head>
<body>
<noscript>
    <strong>
        We're sorry but constructor doesn't work properly without JavaScript enabled. Please enable it to
        continue.
    </strong>
</noscript>
<div id="app"></div>
<script src="/js/chunk-vendors.7ec8c6c4.js"></script>
<script src="/js/app.09e56caf.js"></script>
</body>
</html>
