<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = ['#a1d7de', '#f8b8b0', '#bdb4d1', '#addfb4', '#ffefa7', '#9dd6cb'];

        DB::table('templates')->insert([
            'name'   => 'template1-h',
            'orient' => 'h',
            'group'  => 'g1',
            'bg'     => 'template/template1-h.jpg',
            'src'    => 'templatePreview/template1-h.jpg'
        ]);

        DB::table('templates')->insert([
            'name'   => 'template1-v',
            'orient' => 'v',
            'group'  => 'g1',
            'bg'     => 'template/template1-v.jpg',
            'src'    => 'templatePreview/template1-v.jpg'
        ]);

        DB::table('props')->insert([
            'name'  => 'text1',
            'with'  => 52,
            'pos_x' => 35,
            'pos_y' => 72,
        ]);

        DB::table('props')->insert([
            'name'  => 'text1',
            'with'  => 76,
            'pos_x' => 50,
            'pos_y' => 74,
        ]);

        DB::table('props_templates')->insert([
            'templates_id' => 1,
            'props_id'     => 1,
        ]);

        DB::table('props_templates')->insert([
            'templates_id' => 2,
            'props_id'     => 2,
        ]);

        for ($i = 1; $i <= 5; $i++) {
            DB::table('stickers')->insert([
                'name' => 'sticker' . $i,
                'src'  => 'stickers/el' . $i . '.png',
            ]);
        }

        foreach ($colors as $key => $color) {
            DB::table('colors')->insert([
                'name'  => 'color' . ($key + 1),
                'color' => $color,
            ]);
        }

        for ($i = 1; $i <= 6; $i++) {
            DB::table('texts')->insert([
                'name' => 'text' . $i,
                'src'  => 'text/text' . $i . '.svg',
            ]);
        }
    }
}
